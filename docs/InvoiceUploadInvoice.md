# InvoiceUploadInvoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**no** | **i64** |  | [default to null]
**period** | [***::models::DatePeriod**](DatePeriod.md) |  | [default to null]
**issue_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**tax_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**due_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [optional] [default to null]
**is_monthly_invoice** | **bool** |  | [default to null]
**issued_by** | **String** |  | [optional] [default to null]
**charges** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


