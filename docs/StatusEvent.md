# StatusEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audit_number** | **String** |  | [default to null]
**creation_timestamp** | [**chrono::DateTime<chrono::offset::Utc>**](chrono::DateTime&lt;chrono::offset::Utc&gt;.md) |  | [default to null]
**event_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**is_replaced** | **bool** |  | [default to null]
**is_reversed** | **bool** |  | [default to null]
**icp** | **String** |  | [default to null]
**user_reference** | **String** |  | [default to null]
**status** | **String** |  | [default to null]
**status_reason** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


