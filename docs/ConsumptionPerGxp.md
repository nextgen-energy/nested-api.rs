# ConsumptionPerGxp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gxp** | **String** |  | [default to null]
**network** | **String** |  | [default to null]
**loss_code** | **String** |  | [default to null]
**dedicated_nsp** | **bool** |  | [default to null]
**profile** | **String** |  | [default to null]
**flow_direction** | **String** |  | [default to null]
**day** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**consumption** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


