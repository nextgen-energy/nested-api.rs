# InsertStatusEventsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | [**Vec<::models::StatusEvent>**](StatusEvent.md) | A list of Status events | [default to null]
**token** | **String** | The authentication token | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


