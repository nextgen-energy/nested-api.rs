# IcpNewRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icps** | [**Vec<::models::IcpNewDetails>**](IcpNewDetails.md) | A list of ICPs to create | [default to null]
**token** | **String** | The authentication token | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


