# IcpListEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icp** | **String** |  | [default to null]
**network** | **String** |  | [default to null]
**gxp** | **String** |  | [default to null]
**retailer** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


