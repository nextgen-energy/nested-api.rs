# BillingDataTariff

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [default to null]
**description** | **String** |  | [default to null]
**meter** | **String** |  | [optional] [default to null]
**aggregate** | **bool** |  | [default to null]
**price** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]
**unit** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


