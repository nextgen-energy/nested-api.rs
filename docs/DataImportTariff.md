# DataImportTariff

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **i64** |  | [default to null]
**network** | **String** |  | [default to null]
**code** | **String** |  | [default to null]
**description** | **String** |  | [default to null]
**aggregate** | **bool** |  | [default to null]
**price** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]
**unit** | **String** |  | [default to null]
**start_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**end_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


