# DataImportConnectionTariffAssoc

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **i64** |  | [default to null]
**connection** | **i64** |  | [default to null]
**icp** | **String** |  | [default to null]
**meter_serial_no** | **String** |  | [optional] [default to null]
**meter_register_no** | **i32** |  | [optional] [default to null]
**tariff** | **i64** |  | [default to null]
**start_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**end_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [optional] [default to null]
**manual** | **bool** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


