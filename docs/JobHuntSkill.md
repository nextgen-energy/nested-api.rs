# JobHuntSkill

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_type** | **String** |  | [default to null]
**subjob_types** | **Vec<String>** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


