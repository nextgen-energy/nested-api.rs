# ConnectionTariffInfoEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connection_id** | **i64** |  | [default to null]
**start_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**end_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**icp** | **String** |  | [default to null]
**meter** | [***::models::ConnectionTariffInfoMeter**](ConnectionTariffInfoMeter.md) |  | [optional] [default to null]
**tariff** | **String** |  | [default to null]
**tariff_rate** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]
**network_tariff** | [***::models::NetworkTariffInfo**](NetworkTariffInfo.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


