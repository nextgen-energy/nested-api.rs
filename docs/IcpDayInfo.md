# IcpDayInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retailer** | **String** |  | [default to null]
**profile** | **String** |  | [default to null]
**submission_hhr** | **bool** |  | [default to null]
**submission_nhh** | **bool** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


