# BillingDataRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connection** | **i64** |  | [default to null]
**period** | [***::models::DatePeriod**](DatePeriod.md) |  | [default to null]
**token** | **String** | The authentication token | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


