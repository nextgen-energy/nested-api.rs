# JobPublishRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | The authentication token | [default to null]
**job** | [***::models::JobPublishJob**](JobPublishJob.md) |  | [default to null]
**subjobs** | [**Vec<::models::JobPublishSubjob>**](JobPublishSubjob.md) | The subjobs | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


