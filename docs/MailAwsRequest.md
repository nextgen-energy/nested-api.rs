# MailAwsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_type** | **String** | The request type | [default to null]
**message_id** | **String** | The message id | [default to null]
**topic_arn** | **String** | The AWS SNS Topic ARN | [default to null]
**subject** | **String** | The subject of the notification | [optional] [default to null]
**message** | **String** | The notification message | [default to null]
**timestamp** | [**chrono::DateTime<chrono::offset::Utc>**](chrono::DateTime&lt;chrono::offset::Utc&gt;.md) | Message timestamp | [default to null]
**signature_version** | **String** | The signature version | [default to null]
**signature** | **String** | The signature | [default to null]
**signing_cert_url** | **String** | The signature&#39;s certificate url | [default to null]
**subscribe_url** | **String** | The subscribe URL | [optional] [default to null]
**unsubscribe_url** | **String** | The unsubscribe URL | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


