# DataImportRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customers** | [**Vec<::models::DataImportCustomer>**](DataImportCustomer.md) | A list of Customers | [default to null]
**addresses** | [**Vec<::models::DataImportAddress>**](DataImportAddress.md) | A list of Addresses | [optional] [default to null]
**connections** | [**Vec<::models::DataImportConnection>**](DataImportConnection.md) | A list of Connections | [default to null]
**tariffs** | [**Vec<::models::DataImportTariff>**](DataImportTariff.md) | A list of Tariffs | [default to null]
**tariff_assocs** | [**Vec<::models::DataImportTariffAssoc>**](DataImportTariffAssoc.md) | A list of Tariff Assocs | [default to null]
**connection_tariff_assocs** | [**Vec<::models::DataImportConnectionTariffAssoc>**](DataImportConnectionTariffAssoc.md) | A list of Connection Tariff Assocs | [default to null]
**network_tariffs** | [**Vec<::models::DataImportNetworkTariff>**](DataImportNetworkTariff.md) | A list of Network Tariffs | [optional] [default to null]
**network_tariff_mappings** | [**Vec<::models::DataImportNetworkTariffMapping>**](DataImportNetworkTariffMapping.md) | A list of Network Tariff Mappings | [default to null]
**token** | **String** | The authentication token | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


