# MailDownloadMail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [default to null]
**source** | **String** |  | [default to null]
**date** | [**chrono::DateTime<chrono::offset::Utc>**](chrono::DateTime&lt;chrono::offset::Utc&gt;.md) |  | [default to null]
**subject** | **String** |  | [default to null]
**read_on** | [**chrono::DateTime<chrono::offset::Utc>**](chrono::DateTime&lt;chrono::offset::Utc&gt;.md) |  | [optional] [default to null]
**read_by** | **String** |  | [optional] [default to null]
**body** | **String** |  | [default to null]
**destinations** | **Vec<String>** |  | [default to null]
**headers** | [**Vec<::models::MailDownloadHeader>**](MailDownloadHeader.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


