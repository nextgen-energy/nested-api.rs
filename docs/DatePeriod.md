# DatePeriod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**end** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


