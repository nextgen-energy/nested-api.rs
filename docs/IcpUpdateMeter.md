# IcpUpdateMeter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icp** | **String** |  | [default to null]
**installation_no** | **i32** |  | [default to null]
**serial_no** | **String** |  | [default to null]
**start_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**end_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


