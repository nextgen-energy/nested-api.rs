# \InternalApi

All URIs are relative to *https://staging.nextgen.energy/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**auth_employee**](InternalApi.md#auth_employee) | **Post** /auth/employee | Authenticate employee
[**auth_employee_teams**](InternalApi.md#auth_employee_teams) | **Post** /auth/teams | List all Teams of an Employee
[**check_auth_token**](InternalApi.md#check_auth_token) | **Post** /auth/check | Check authentication token
[**connection_tariff_info**](InternalApi.md#connection_tariff_info) | **Post** /connection/tariff_info | Get tariff info for a list of icps
[**employee_list**](InternalApi.md#employee_list) | **Post** /employee/list | List employees
[**events_list**](InternalApi.md#events_list) | **Post** /events/list | List all events for a list of ICPs
[**file_download**](InternalApi.md#file_download) | **Post** /file/download | Download a processed file
[**file_list**](InternalApi.md#file_list) | **Post** /file/list | List Files
[**file_upload**](InternalApi.md#file_upload) | **Post** /file/upload | Upload already processed File
[**hhr_data**](InternalApi.md#hhr_data) | **Post** /hhr/data | Query HHR Data
[**icp_details**](InternalApi.md#icp_details) | **Post** /icp/details | Details for a list of ICPs
[**icp_list**](InternalApi.md#icp_list) | **Post** /icp/list | List of all ICPs
[**icp_metering**](InternalApi.md#icp_metering) | **Post** /icp/metering | Metering details for a list of ICPs
[**invoice_list**](InternalApi.md#invoice_list) | **Post** /invoice/list | List Invoices
[**job_enqueue**](InternalApi.md#job_enqueue) | **Post** /jobs/enqueue | Enqueue a new job
[**job_status**](InternalApi.md#job_status) | **Post** /jobs/status | Query the status of a job
[**mail_download**](InternalApi.md#mail_download) | **Post** /mail/download | Download an Email
[**mail_mark_read**](InternalApi.md#mail_mark_read) | **Post** /mail/read | Mark an Email as read
[**mail_send**](InternalApi.md#mail_send) | **Post** /mail/send | Send an Email
[**pending_files**](InternalApi.md#pending_files) | **Post** /file/pending | Get a list of pending/errored files
[**reads_query**](InternalApi.md#reads_query) | **Post** /reads/query | Query reads


# **auth_employee**
> ::models::AuthEmployeeResponse auth_employee(body)
Authenticate employee

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**AuthEmployeeRequest**](AuthEmployeeRequest.md)|  | 

### Return type

[**::models::AuthEmployeeResponse**](AuthEmployeeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **auth_employee_teams**
> ::models::AuthEmployeeTeamsResponse auth_employee_teams(body)
List all Teams of an Employee

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**AuthEmployeeTeamsRequest**](AuthEmployeeTeamsRequest.md)|  | 

### Return type

[**::models::AuthEmployeeTeamsResponse**](AuthEmployeeTeamsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **check_auth_token**
> ::models::AuthCheckResponse check_auth_token(body)
Check authentication token

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest**](TokenOnlyRequest.md)|  | 

### Return type

[**::models::AuthCheckResponse**](AuthCheckResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connection_tariff_info**
> ::models::ConnectionTariffInfoResponse connection_tariff_info(body)
Get tariff info for a list of icps

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**ConnectionTariffInfoRequest**](ConnectionTariffInfoRequest.md)|  | 

### Return type

[**::models::ConnectionTariffInfoResponse**](ConnectionTariffInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **employee_list**
> ::models::EmployeeListResponse employee_list(body)
List employees

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest2**](TokenOnlyRequest2.md)|  | 

### Return type

[**::models::EmployeeListResponse**](EmployeeListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **events_list**
> ::models::EventList events_list(body)
List all events for a list of ICPs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**EventsListRequest**](EventsListRequest.md)|  | 

### Return type

[**::models::EventList**](EventList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **file_download**
> ::models::FileDownloadResponse file_download(body)
Download a processed file

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**FileDownloadRequest**](FileDownloadRequest.md)|  | 

### Return type

[**::models::FileDownloadResponse**](FileDownloadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **file_list**
> ::models::FileListResponse file_list(body)
List Files

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**FileListRequest**](FileListRequest.md)|  | 

### Return type

[**::models::FileListResponse**](FileListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **file_upload**
> ::models::FileUploadResponse file_upload(body)
Upload already processed File

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**FileUploadRequest**](FileUploadRequest.md)|  | 

### Return type

[**::models::FileUploadResponse**](FileUploadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hhr_data**
> ::models::HhrDataResponse hhr_data(body)
Query HHR Data

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**HhrDataRequest**](HhrDataRequest.md)|  | 

### Return type

[**::models::HhrDataResponse**](HHRDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **icp_details**
> ::models::IcpDetailsResponse icp_details(body)
Details for a list of ICPs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**IcpDetailsRequest**](IcpDetailsRequest.md)|  | 

### Return type

[**::models::IcpDetailsResponse**](IcpDetailsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **icp_list**
> ::models::IcpListResponse icp_list(body)
List of all ICPs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest4**](TokenOnlyRequest4.md)|  | 

### Return type

[**::models::IcpListResponse**](IcpListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **icp_metering**
> ::models::IcpMeteringResponse icp_metering(body)
Metering details for a list of ICPs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**IcpMeteringRequest**](IcpMeteringRequest.md)|  | 

### Return type

[**::models::IcpMeteringResponse**](IcpMeteringResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **invoice_list**
> ::models::InvoiceList invoice_list(body)
List Invoices

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**InvoiceListRequest**](InvoiceListRequest.md)|  | 

### Return type

[**::models::InvoiceList**](InvoiceList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **job_enqueue**
> ::models::JobEnqueueResponse job_enqueue(body)
Enqueue a new job

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**JobEnqueueRequest**](JobEnqueueRequest.md)|  | 

### Return type

[**::models::JobEnqueueResponse**](JobEnqueueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **job_status**
> ::models::JobStatusResponse job_status(body)
Query the status of a job

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**JobStatusRequest**](JobStatusRequest.md)|  | 

### Return type

[**::models::JobStatusResponse**](JobStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mail_download**
> ::models::MailDownloadResponse mail_download(body)
Download an Email

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**MailDownloadRequest**](MailDownloadRequest.md)|  | 

### Return type

[**::models::MailDownloadResponse**](MailDownloadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mail_mark_read**
> ::models::ErrorOnlyResponse mail_mark_read(body)
Mark an Email as read

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**MailMarkReadRequest**](MailMarkReadRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mail_send**
> ::models::ErrorOnlyResponse mail_send(body)
Send an Email

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**MailSendRequest**](MailSendRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pending_files**
> ::models::FilesPendingResponse pending_files(body)
Get a list of pending/errored files

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest3**](TokenOnlyRequest3.md)|  | 

### Return type

[**::models::FilesPendingResponse**](FilesPendingResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reads_query**
> ::models::ReadsQueryResponse reads_query(body)
Query reads

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**ReadsQueryRequest**](ReadsQueryRequest.md)|  | 

### Return type

[**::models::ReadsQueryResponse**](ReadsQueryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

