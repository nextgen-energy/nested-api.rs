# \ServiceApi

All URIs are relative to *https://staging.nextgen.energy/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**auth_service**](ServiceApi.md#auth_service) | **Post** /auth/service | Authenticate service
[**billing_data**](ServiceApi.md#billing_data) | **Post** /billing/data | Get Data for Billing
[**billing_queue**](ServiceApi.md#billing_queue) | **Post** /billing/queue | Get Billing Queue
[**check_auth_token**](ServiceApi.md#check_auth_token) | **Post** /auth/check | Check authentication token
[**connection_tariff_info**](ServiceApi.md#connection_tariff_info) | **Post** /connection/tariff_info | Get tariff info for a list of icps
[**consumption_import**](ServiceApi.md#consumption_import) | **Post** /consumption/import | Import Historic Consumption from ETS
[**consumption_over_period**](ServiceApi.md#consumption_over_period) | **Post** /consumption/period | Return Consumption for the given Period
[**consumption_per_gxp**](ServiceApi.md#consumption_per_gxp) | **Post** /consumption/gxp | Return Consumption for the given Period per GXP
[**data_import**](ServiceApi.md#data_import) | **Post** /data/import | Import Data
[**employee_list**](ServiceApi.md#employee_list) | **Post** /employee/list | List employees
[**events_list**](ServiceApi.md#events_list) | **Post** /events/list | List all events for a list of ICPs
[**file_download**](ServiceApi.md#file_download) | **Post** /file/download | Download a processed file
[**file_list**](ServiceApi.md#file_list) | **Post** /file/list | List Files
[**file_update**](ServiceApi.md#file_update) | **Post** /file/update | Update a processed file
[**file_upload**](ServiceApi.md#file_upload) | **Post** /file/upload | Upload already processed File
[**hhr_data**](ServiceApi.md#hhr_data) | **Post** /hhr/data | Query HHR Data
[**hhr_data_per_gxp**](ServiceApi.md#hhr_data_per_gxp) | **Post** /hhr/gxp | Return HHR Data for the given Period per GXP
[**hhr_history**](ServiceApi.md#hhr_history) | **Post** /hhr/history | Query HHR History
[**hhr_upload**](ServiceApi.md#hhr_upload) | **Post** /hhr/upload | Upload or update HHR data
[**icp_days**](ServiceApi.md#icp_days) | **Post** /icp/days | List of information per ICP day
[**icp_details**](ServiceApi.md#icp_details) | **Post** /icp/details | Details for a list of ICPs
[**icp_list**](ServiceApi.md#icp_list) | **Post** /icp/list | List of all ICPs
[**icp_metering**](ServiceApi.md#icp_metering) | **Post** /icp/metering | Metering details for a list of ICPs
[**icp_new**](ServiceApi.md#icp_new) | **Post** /icp/new | Insert new ICPs
[**icp_update**](ServiceApi.md#icp_update) | **Post** /icp/update | Update an ICP
[**insert_cs_events**](ServiceApi.md#insert_cs_events) | **Post** /events/cs | Insert a list of CS events
[**insert_status_events**](ServiceApi.md#insert_status_events) | **Post** /events/status | Insert a list of Status events
[**insert_trader_events**](ServiceApi.md#insert_trader_events) | **Post** /events/trader | Insert a list of Trader events
[**invoice_list**](ServiceApi.md#invoice_list) | **Post** /invoice/list | List Invoices
[**invoice_no**](ServiceApi.md#invoice_no) | **Post** /invoice/no | Next Invoice No
[**invoice_upload**](ServiceApi.md#invoice_upload) | **Post** /invoice/upload | Upload Invoice
[**job_executed**](ServiceApi.md#job_executed) | **Post** /jobs/executed | Mark a subjob as executed
[**job_hunt**](ServiceApi.md#job_hunt) | **Post** /jobs/hunt | Hunt for a job
[**job_publish**](ServiceApi.md#job_publish) | **Post** /jobs/publish | Publish subjobs for an analyzed or new job
[**mail_download**](ServiceApi.md#mail_download) | **Post** /mail/download | Download an Email
[**mail_mark_read**](ServiceApi.md#mail_mark_read) | **Post** /mail/read | Mark an Email as read
[**mail_pending**](ServiceApi.md#mail_pending) | **Post** /mail/pending | List Emails that are pending to be sent
[**mail_send**](ServiceApi.md#mail_send) | **Post** /mail/send | Send an Email
[**pending_files**](ServiceApi.md#pending_files) | **Post** /file/pending | Get a list of pending/errored files
[**reads_query**](ServiceApi.md#reads_query) | **Post** /reads/query | Query reads
[**reads_upload**](ServiceApi.md#reads_upload) | **Post** /reads/upload | Upload or update meter reads
[**upload_shape_profile**](ServiceApi.md#upload_shape_profile) | **Post** /shape/upload | Upload shape profile


# **auth_service**
> ::models::AuthEmployeeResponse auth_service(body)
Authenticate service

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**AuthServiceRequest**](AuthServiceRequest.md)|  | 

### Return type

[**::models::AuthEmployeeResponse**](AuthEmployeeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_data**
> ::models::BillingDataResponse billing_data(body)
Get Data for Billing

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**BillingDataRequest**](BillingDataRequest.md)|  | 

### Return type

[**::models::BillingDataResponse**](BillingDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **billing_queue**
> ::models::BillingQueueResponse billing_queue(body)
Get Billing Queue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest1**](TokenOnlyRequest1.md)|  | 

### Return type

[**::models::BillingQueueResponse**](BillingQueueResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **check_auth_token**
> ::models::AuthCheckResponse check_auth_token(body)
Check authentication token

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest**](TokenOnlyRequest.md)|  | 

### Return type

[**::models::AuthCheckResponse**](AuthCheckResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **connection_tariff_info**
> ::models::ConnectionTariffInfoResponse connection_tariff_info(body)
Get tariff info for a list of icps

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**ConnectionTariffInfoRequest**](ConnectionTariffInfoRequest.md)|  | 

### Return type

[**::models::ConnectionTariffInfoResponse**](ConnectionTariffInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **consumption_import**
> ::models::ErrorOnlyResponse consumption_import(body)
Import Historic Consumption from ETS

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**ConsumptionImportRequest**](ConsumptionImportRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **consumption_over_period**
> ::models::ConsumptionOverPeriodResponse consumption_over_period(body)
Return Consumption for the given Period

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**ConsumptionOverPeriodRequest**](ConsumptionOverPeriodRequest.md)|  | 

### Return type

[**::models::ConsumptionOverPeriodResponse**](ConsumptionOverPeriodResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **consumption_per_gxp**
> ::models::ConsumptionPerGxpResponse consumption_per_gxp(body)
Return Consumption for the given Period per GXP

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**ConsumptionPerGxpRequest**](ConsumptionPerGxpRequest.md)|  | 

### Return type

[**::models::ConsumptionPerGxpResponse**](ConsumptionPerGxpResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **data_import**
> ::models::ErrorOnlyResponse data_import(body)
Import Data

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**DataImportRequest**](DataImportRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **employee_list**
> ::models::EmployeeListResponse employee_list(body)
List employees

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest2**](TokenOnlyRequest2.md)|  | 

### Return type

[**::models::EmployeeListResponse**](EmployeeListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **events_list**
> ::models::EventList events_list(body)
List all events for a list of ICPs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**EventsListRequest**](EventsListRequest.md)|  | 

### Return type

[**::models::EventList**](EventList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **file_download**
> ::models::FileDownloadResponse file_download(body)
Download a processed file

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**FileDownloadRequest**](FileDownloadRequest.md)|  | 

### Return type

[**::models::FileDownloadResponse**](FileDownloadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **file_list**
> ::models::FileListResponse file_list(body)
List Files

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**FileListRequest**](FileListRequest.md)|  | 

### Return type

[**::models::FileListResponse**](FileListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **file_update**
> ::models::ErrorOnlyResponse file_update(body)
Update a processed file

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**FileUpdateRequest**](FileUpdateRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **file_upload**
> ::models::FileUploadResponse file_upload(body)
Upload already processed File

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**FileUploadRequest**](FileUploadRequest.md)|  | 

### Return type

[**::models::FileUploadResponse**](FileUploadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hhr_data**
> ::models::HhrDataResponse hhr_data(body)
Query HHR Data

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**HhrDataRequest**](HhrDataRequest.md)|  | 

### Return type

[**::models::HhrDataResponse**](HHRDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hhr_data_per_gxp**
> ::models::HhrDataPerGxpResponse hhr_data_per_gxp(body)
Return HHR Data for the given Period per GXP

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**HhrDataPerGxpRequest**](HhrDataPerGxpRequest.md)|  | 

### Return type

[**::models::HhrDataPerGxpResponse**](HHRDataPerGxpResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hhr_history**
> ::models::HhrHistoryResponse hhr_history(body)
Query HHR History

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**HhrHistoryRequest**](HhrHistoryRequest.md)|  | 

### Return type

[**::models::HhrHistoryResponse**](HHRHistoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hhr_upload**
> ::models::ErrorOnlyResponse hhr_upload(body)
Upload or update HHR data

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**HhrUploadRequest**](HhrUploadRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **icp_days**
> ::models::IcpDaysResponse icp_days(body)
List of information per ICP day

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**IcpDaysRequest**](IcpDaysRequest.md)|  | 

### Return type

[**::models::IcpDaysResponse**](IcpDaysResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **icp_details**
> ::models::IcpDetailsResponse icp_details(body)
Details for a list of ICPs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**IcpDetailsRequest**](IcpDetailsRequest.md)|  | 

### Return type

[**::models::IcpDetailsResponse**](IcpDetailsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **icp_list**
> ::models::IcpListResponse icp_list(body)
List of all ICPs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest4**](TokenOnlyRequest4.md)|  | 

### Return type

[**::models::IcpListResponse**](IcpListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **icp_metering**
> ::models::IcpMeteringResponse icp_metering(body)
Metering details for a list of ICPs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**IcpMeteringRequest**](IcpMeteringRequest.md)|  | 

### Return type

[**::models::IcpMeteringResponse**](IcpMeteringResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **icp_new**
> ::models::ErrorOnlyResponse icp_new(body)
Insert new ICPs

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**IcpNewRequest**](IcpNewRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **icp_update**
> ::models::ErrorOnlyResponse icp_update(body)
Update an ICP

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**IcpUpdateRequest**](IcpUpdateRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **insert_cs_events**
> ::models::ErrorOnlyResponse insert_cs_events(body)
Insert a list of CS events

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**InsertCsEventsRequest**](InsertCsEventsRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **insert_status_events**
> ::models::ErrorOnlyResponse insert_status_events(body)
Insert a list of Status events

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**InsertStatusEventsRequest**](InsertStatusEventsRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **insert_trader_events**
> ::models::ErrorOnlyResponse insert_trader_events(body)
Insert a list of Trader events

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**InsertTraderEventsRequest**](InsertTraderEventsRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **invoice_list**
> ::models::InvoiceList invoice_list(body)
List Invoices

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**InvoiceListRequest**](InvoiceListRequest.md)|  | 

### Return type

[**::models::InvoiceList**](InvoiceList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **invoice_no**
> ::models::InvoiceNoResponse invoice_no(body)
Next Invoice No

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest5**](TokenOnlyRequest5.md)|  | 

### Return type

[**::models::InvoiceNoResponse**](InvoiceNoResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **invoice_upload**
> ::models::ErrorOnlyResponse invoice_upload(body)
Upload Invoice

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**InvoiceUploadRequest**](InvoiceUploadRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **job_executed**
> job_executed(body)
Mark a subjob as executed

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**JobExecutedRequest**](JobExecutedRequest.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **job_hunt**
> ::models::JobHuntResponse job_hunt(body)
Hunt for a job

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**JobHuntRequest**](JobHuntRequest.md)|  | 

### Return type

[**::models::JobHuntResponse**](JobHuntResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **job_publish**
> job_publish(body)
Publish subjobs for an analyzed or new job

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**JobPublishRequest**](JobPublishRequest.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mail_download**
> ::models::MailDownloadResponse mail_download(body)
Download an Email

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**MailDownloadRequest**](MailDownloadRequest.md)|  | 

### Return type

[**::models::MailDownloadResponse**](MailDownloadResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mail_mark_read**
> ::models::ErrorOnlyResponse mail_mark_read(body)
Mark an Email as read

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**MailMarkReadRequest**](MailMarkReadRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mail_pending**
> ::models::MailPendingResponse mail_pending(body)
List Emails that are pending to be sent

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest6**](TokenOnlyRequest6.md)|  | 

### Return type

[**::models::MailPendingResponse**](MailPendingResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **mail_send**
> ::models::ErrorOnlyResponse mail_send(body)
Send an Email

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**MailSendRequest**](MailSendRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pending_files**
> ::models::FilesPendingResponse pending_files(body)
Get a list of pending/errored files

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest3**](TokenOnlyRequest3.md)|  | 

### Return type

[**::models::FilesPendingResponse**](FilesPendingResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reads_query**
> ::models::ReadsQueryResponse reads_query(body)
Query reads

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**ReadsQueryRequest**](ReadsQueryRequest.md)|  | 

### Return type

[**::models::ReadsQueryResponse**](ReadsQueryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reads_upload**
> ::models::ErrorOnlyResponse reads_upload(body)
Upload or update meter reads

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**ReadsUploadRequest**](ReadsUploadRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_shape_profile**
> ::models::ErrorOnlyResponse upload_shape_profile(body)
Upload shape profile

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**ShapeUploadRequest**](ShapeUploadRequest.md)|  | 

### Return type

[**::models::ErrorOnlyResponse**](ErrorOnlyResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

