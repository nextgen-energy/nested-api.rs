# JobStatusSubjobs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **i64** |  | [default to null]
**pending** | **i64** |  | [default to null]
**failed** | **i64** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


