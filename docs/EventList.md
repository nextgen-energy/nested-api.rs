# EventList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **String** | Error message, or null if there was no error | [optional] [default to null]
**cs** | [**Vec<::models::CsEvent>**](CSEvent.md) | List of CS events | [optional] [default to null]
**status** | [**Vec<::models::StatusEvent>**](StatusEvent.md) | List of Status events | [optional] [default to null]
**trader** | [**Vec<::models::TraderEvent>**](TraderEvent.md) | List of Trader events | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


