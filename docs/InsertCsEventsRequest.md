# InsertCsEventsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | [**Vec<::models::CsEvent>**](CSEvent.md) | A list of CS events | [default to null]
**installations** | [**Vec<::models::CsEventInstallation>**](CSEventInstallation.md) | A list of Installation Information from CS files | [default to null]
**token** | **String** | The authentication token | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


