# BillingDataDay

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**consumption** | [**::std::collections::HashMap<String, ::models::BigDecimal>**](BigDecimal.md) |  | [default to null]
**tariffs** | [**Vec<::models::BillingDataTariff>**](BillingDataTariff.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


