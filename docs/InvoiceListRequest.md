# InvoiceListRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**connections** | **Vec<i64>** | The connections for which the invoices should be returned | [default to null]
**token** | **String** | The authentication token | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


