# FileDownloadResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **String** | The content of the file (might be null for empty files) | [optional] [default to null]
**filename** | **String** | The filename of the file | [optional] [default to null]
**filetype** | **String** | The type of the file | [optional] [default to null]
**error** | **String** | Error message, or null if there was no error | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


