# JobHuntJob

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**worker_token** | [***uuid::Uuid**](uuid::Uuid.md) |  | [default to null]
**id** | **i64** |  | [default to null]
**status** | [***::models::JobHuntJobStatus**](JobHuntJobStatus.md) |  | [default to null]
**metadata** | **::std::collections::HashMap<String, String>** |  | [default to null]
**job_type** | **String** | The type of the job or subjob | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


