# FileUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **i64** |  | [default to null]
**import_completed_at** | [**chrono::DateTime<chrono::offset::Utc>**](chrono::DateTime&lt;chrono::offset::Utc&gt;.md) |  | [optional] [default to null]
**import_errors** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


