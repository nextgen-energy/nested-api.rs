# BillingData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_name** | **String** |  | [default to null]
**customer_address** | [***::models::Address**](Address.md) |  | [default to null]
**address_unit** | **String** |  | [default to null]
**address_number** | **String** |  | [default to null]
**address_street** | **String** |  | [default to null]
**address_suburb** | **String** |  | [default to null]
**address_town** | **String** |  | [default to null]
**address_post_code** | **i32** |  | [default to null]
**icp** | **String** |  | [default to null]
**balance** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]
**days** | [**Vec<::models::BillingDataDay>**](BillingDataDay.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


