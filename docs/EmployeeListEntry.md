# EmployeeListEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | [default to null]
**firstname** | **String** |  | [default to null]
**middlename** | **String** |  | [optional] [default to null]
**lastname** | **String** |  | [default to null]
**teams** | [**Vec<::models::Team>**](Team.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


