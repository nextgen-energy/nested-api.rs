# DataImportTariffAssoc

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **i64** |  | [default to null]
**network** | **String** |  | [default to null]
**tariff_class** | **String** |  | [optional] [default to null]
**content_code** | **String** |  | [optional] [default to null]
**capacity_min** | **i32** |  | [optional] [default to null]
**capacity_max** | **i32** |  | [optional] [default to null]
**tariff** | **i64** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


