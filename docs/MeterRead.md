# MeterRead

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icp** | **String** |  | [default to null]
**meter_serial_no** | **String** |  | [default to null]
**meter_register_no** | **i32** |  | [default to null]
**read_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**read** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]
**estimate** | **bool** |  | [default to null]
**comment** | **String** |  | [default to null]
**ignored** | **bool** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


