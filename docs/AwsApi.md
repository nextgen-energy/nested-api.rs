# \AwsApi

All URIs are relative to *https://staging.nextgen.energy/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**mail_aws_sns_notification**](AwsApi.md#mail_aws_sns_notification) | **Post** /mail/aws | Post mail-related AWS SNS notification


# **mail_aws_sns_notification**
> mail_aws_sns_notification(body)
Post mail-related AWS SNS notification

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**MailAwsRequest**](MailAwsRequest.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

