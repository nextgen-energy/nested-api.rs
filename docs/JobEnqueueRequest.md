# JobEnqueueRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | The authentication token | [default to null]
**job_type** | **String** | The type of the job | [default to null]
**metadata** | [**Vec<::models::JobEnqueueMetadata>**](JobEnqueueMetadata.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


