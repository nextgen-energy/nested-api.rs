# FileUploadRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **String** | The file body | [default to null]
**info** | [***::models::FileUploadInfo**](FileUploadInfo.md) |  | [default to null]
**token** | **String** | The authentication token | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


