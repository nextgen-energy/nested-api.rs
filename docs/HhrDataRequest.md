# HhrDataRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | The authentication token | [default to null]
**start** | [***chrono::NaiveDate**](chrono::NaiveDate.md) | The first date to return HHR data for. | [default to null]
**end** | [***chrono::NaiveDate**](chrono::NaiveDate.md) | The last date to return HHR data for. | [default to null]
**registers** | [**Vec<::models::HhrDataRegister>**](HHRDataRegister.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


