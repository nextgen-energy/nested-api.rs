# ReadsQueryRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | The authentication token | [default to null]
**icps** | **Vec<String>** | A list of ICPs | [default to null]
**start** | [***chrono::NaiveDate**](chrono::NaiveDate.md) | The first date to return a read for | [default to null]
**end** | [***chrono::NaiveDate**](chrono::NaiveDate.md) | The last date to return a read for | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


