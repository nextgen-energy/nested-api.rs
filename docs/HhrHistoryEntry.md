# HhrHistoryEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**network** | **String** |  | [default to null]
**gxp** | **String** |  | [default to null]
**content_code** | **String** |  | [default to null]
**average_consumption** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]
**month** | **i32** |  | [default to null]
**timeframe** | **i32** |  | [default to null]
**timeframe_consumption** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


