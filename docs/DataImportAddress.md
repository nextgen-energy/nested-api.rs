# DataImportAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **i64** |  | [default to null]
**customer** | **i64** |  | [default to null]
**street_number** | **i32** |  | [default to null]
**street_alpha** | **String** |  | [default to null]
**unit_value** | **String** |  | [default to null]
**street** | **String** |  | [default to null]
**post_code** | **i32** |  | [default to null]
**suburb** | **String** |  | [default to null]
**city** | **String** |  | [default to null]
**start_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**end_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


