# IcpUpdateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**Vec<::models::IcpUpdateAddress>**](IcpUpdateAddress.md) | ICP Address updates | [default to null]
**details** | [**Vec<::models::IcpUpdateDetails>**](IcpUpdateDetails.md) | ICP Details updates | [default to null]
**installations** | [**Vec<::models::IcpUpdateInstallation>**](IcpUpdateInstallation.md) | Meter Installation updates | [default to null]
**meters** | [**Vec<::models::IcpUpdateMeter>**](IcpUpdateMeter.md) | Meter updates | [default to null]
**registers** | [**Vec<::models::IcpUpdateRegister>**](IcpUpdateRegister.md) | Meter register updates | [default to null]
**token** | **String** | The authentication token | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


