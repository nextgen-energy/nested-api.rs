# HhrDataRegisterEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**register** | [***::models::HhrDataRegister**](HHRDataRegister.md) |  | [default to null]
**data** | [**Vec<::models::HhrDataEntry>**](HHRDataEntry.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


