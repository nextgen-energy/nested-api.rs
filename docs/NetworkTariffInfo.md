# NetworkTariffInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**network_tariff_code** | **String** |  | [default to null]
**price** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]
**unit** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


