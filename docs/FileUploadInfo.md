# FileUploadInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **String** |  | [default to null]
**filetype** | **String** |  | [default to null]
**sender** | **String** |  | [default to null]
**recipient** | **String** |  | [default to null]
**date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**time** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


