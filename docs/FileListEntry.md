# FileListEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **i64** |  | [default to null]
**filename** | **String** |  | [default to null]
**filetype** | **String** |  | [default to null]
**sender** | **String** |  | [default to null]
**recipient** | **String** |  | [default to null]
**date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**time** | **String** |  | [default to null]
**import_completed_at** | [**chrono::DateTime<chrono::offset::Utc>**](chrono::DateTime&lt;chrono::offset::Utc&gt;.md) |  | [optional] [default to null]
**import_errors** | **String** |  | [optional] [default to null]
**retry** | **bool** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


