# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**post_code** | **i32** |  | [default to null]
**street_number** | **i32** |  | [default to null]
**street_alpha** | **String** |  | [default to null]
**unit_value** | **String** |  | [default to null]
**road_name** | **String** |  | [default to null]
**road_type** | **String** |  | [default to null]
**road_suffix** | **String** |  | [default to null]
**suburb** | **String** |  | [default to null]
**town** | **String** |  | [default to null]
**dpid** | **i64** |  | [optional] [default to null]
**deliverable** | **bool** |  | [optional] [default to null]
**physical** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


