# ShapeUploadEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gxp** | **String** |  | [default to null]
**network** | **String** |  | [default to null]
**profile** | **String** |  | [default to null]
**day** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**timeframe** | **i32** |  | [default to null]
**load** | **i64** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


