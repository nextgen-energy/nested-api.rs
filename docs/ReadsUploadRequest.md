# ReadsUploadRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reads** | [**Vec<::models::ReadsUploadRead>**](ReadsUploadRead.md) | The meter reads | [default to null]
**token** | **String** | The authentication token | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


