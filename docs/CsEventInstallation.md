# CsEventInstallation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icp** | **String** |  | [default to null]
**installation_no** | **i32** |  | [default to null]
**average_daily_consumption** | **i32** |  | [default to null]
**key_held** | **bool** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


