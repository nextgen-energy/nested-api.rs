# InvoiceUploadRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | The authentication token | [default to null]
**connection** | **i64** |  | [default to null]
**invoice** | [***::models::InvoiceUploadInvoice**](InvoiceUploadInvoice.md) |  | [default to null]
**pdf** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


