# IcpUpdateDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icp** | **String** |  | [default to null]
**gxp** | **String** |  | [default to null]
**price_code** | **String** |  | [default to null]
**dedicated_nsp** | **bool** |  | [default to null]
**loss_code** | **String** |  | [default to null]
**anzsic** | **String** |  | [default to null]
**retailer** | **String** |  | [default to null]
**profile** | **String** |  | [default to null]
**submission_hhr** | **bool** |  | [default to null]
**submission_nhh** | **bool** |  | [default to null]
**proposed_mep** | **String** |  | [default to null]
**mep** | **String** |  | [default to null]
**no_of_installations** | **i32** |  | [default to null]
**installation_type** | **String** |  | [default to null]
**generation_capacity** | [***::models::BigDecimal**](BigDecimal.md) |  | [optional] [default to null]
**capacity** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


