# JobStatusInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | [default to null]
**log** | **String** |  | [default to null]
**subjobs** | [***::models::JobStatusSubjobs**](JobStatusSubjobs.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


