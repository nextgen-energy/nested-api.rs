# ConsumptionPerGxpRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**start_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**token** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


