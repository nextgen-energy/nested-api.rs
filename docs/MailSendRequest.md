# MailSendRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | The authentication token | [default to null]
**source** | **String** | The source of the mail (sender) | [default to null]
**destinations** | [**Vec<::models::MailSendDestination>**](MailSendDestination.md) | The destinations of the mail (recipients) | [default to null]
**headers** | [**Vec<::models::MailDownloadHeader>**](MailDownloadHeader.md) | The headers of the mail (to/from/subject etc must not be present, content-type should be present) | [default to null]
**subject** | **String** | The subject of the mail | [default to null]
**mime_body** | **String** | The (mime) body of the mail | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


