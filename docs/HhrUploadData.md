# HhrUploadData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icp** | **String** |  | [default to null]
**meter_serial_no** | **String** |  | [default to null]
**meter_register_no** | **i32** |  | [default to null]
**date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**timeframe** | **i32** |  | [default to null]
**status** | **String** |  | [default to null]
**consumption** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


