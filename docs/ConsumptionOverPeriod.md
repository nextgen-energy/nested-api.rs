# ConsumptionOverPeriod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icp** | **String** |  | [default to null]
**serial_no** | **String** |  | [default to null]
**register_no** | **i32** |  | [default to null]
**consumption** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]
**profile** | **String** |  | [default to null]
**content_code** | **String** |  | [default to null]
**period_of_availability** | **i32** |  | [default to null]
**flow_direction** | **String** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


