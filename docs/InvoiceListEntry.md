# InvoiceListEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice_no** | **i64** |  | [default to null]
**connection** | **i64** |  | [default to null]
**period** | [***::models::DatePeriod**](DatePeriod.md) |  | [default to null]
**issue_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**tax_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [default to null]
**due_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [optional] [default to null]
**is_monthly_invoice** | **bool** |  | [default to null]
**issued_by** | **String** |  | [optional] [default to null]
**charges** | [***::models::BigDecimal**](BigDecimal.md) |  | [default to null]
**credited_by** | **String** |  | [optional] [default to null]
**credited_on** | [**chrono::DateTime<chrono::offset::Utc>**](chrono::DateTime&lt;chrono::offset::Utc&gt;.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


