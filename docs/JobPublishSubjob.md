# JobPublishSubjob

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subjob_type** | **String** |  | [default to null]
**metadata** | [**Vec<::models::JobPublishMetadata>**](JobPublishMetadata.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


