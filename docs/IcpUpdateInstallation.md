# IcpUpdateInstallation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icp** | **String** |  | [default to null]
**installation_no** | **i32** |  | [default to null]
**meter_location** | **String** |  | [default to null]
**certification_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [optional] [default to null]
**certification_expiry** | [***chrono::NaiveDate**](chrono::NaiveDate.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


