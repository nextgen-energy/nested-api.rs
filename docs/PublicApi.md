# \PublicApi

All URIs are relative to *https://staging.nextgen.energy/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**check_auth_token**](PublicApi.md#check_auth_token) | **Post** /auth/check | Check authentication token


# **check_auth_token**
> ::models::AuthCheckResponse check_auth_token(body)
Check authentication token

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **body** | [**TokenOnlyRequest**](TokenOnlyRequest.md)|  | 

### Return type

[**::models::AuthCheckResponse**](AuthCheckResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

