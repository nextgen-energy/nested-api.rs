# ConnectionTariffInfoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **String** | Error message, or null if there was no error | [optional] [default to null]
**info** | [**Vec<::models::ConnectionTariffInfoEntry>**](ConnectionTariffInfoEntry.md) | The list of connection tariff info | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


