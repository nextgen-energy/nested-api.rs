# IcpDaysRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**icp** | **String** | The ICP | [default to null]
**start_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) | First day | [default to null]
**end_date** | [***chrono::NaiveDate**](chrono::NaiveDate.md) | Last day | [default to null]
**token** | **String** | The authentication token | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


