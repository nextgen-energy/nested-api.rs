# IcpMeteringResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **String** | Error message, or null if there was no error | [optional] [default to null]
**metering** | [**::std::collections::HashMap<String, Vec<Value>>**](array.md) | The metering details | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


