# JobExecutedRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** | The authentication token | [default to null]
**id** | **i64** | The subjob id | [default to null]
**worker_token** | [***uuid::Uuid**](uuid::Uuid.md) | The worker token | [default to null]
**log** | **String** | Execute log | [optional] [default to null]
**success** | **bool** | Whether or not the subjob was successfull | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


