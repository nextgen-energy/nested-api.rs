# JobPublishJob

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_type** | **String** | Either existing_job or new_job | [default to null]
**id** | **i64** | The id of the existing job. Required if type&#x3D;existing_job. | [optional] [default to null]
**worker_token** | [***uuid::Uuid**](uuid::Uuid.md) | The worker_token for the existing job. Required if type&#x3D;existing_job. | [optional] [default to null]
**job_type** | **String** | The type of the new job. Required if type&#x3D;new_job. | [optional] [default to null]
**metadata** | [**Vec<::models::JobPublishMetadata>**](JobPublishMetadata.md) | The metadata of the new job. Required if type&#x3D;new_job. | [optional] [default to null]
**log** | **String** | Log of the analyze phase | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


