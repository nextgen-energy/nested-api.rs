/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

/// FileUploadInfo : The file info

#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct FileUploadInfo {
  #[serde(rename = "filename")]
  filename: String,
  #[serde(rename = "filetype")]
  filetype: String,
  #[serde(rename = "sender")]
  sender: String,
  #[serde(rename = "recipient")]
  recipient: String,
  #[serde(rename = "date")]
  date: chrono::NaiveDate,
  #[serde(rename = "time")]
  time: chrono::NaiveTime
}

impl FileUploadInfo {
  /// The file info
  pub fn new(filename: String, filetype: String, sender: String, recipient: String, date: chrono::NaiveDate, time: chrono::NaiveTime) -> FileUploadInfo {
    FileUploadInfo {
      filename: filename,
      filetype: filetype,
      sender: sender,
      recipient: recipient,
      date: date,
      time: time
    }
  }

  pub fn set_filename(&mut self, filename: String) {
    self.filename = filename;
  }

  pub fn with_filename(mut self, filename: String) -> FileUploadInfo {
    self.filename = filename;
    self
  }

  pub fn filename(&self) -> &String {
    &self.filename
  }


  pub fn set_filetype(&mut self, filetype: String) {
    self.filetype = filetype;
  }

  pub fn with_filetype(mut self, filetype: String) -> FileUploadInfo {
    self.filetype = filetype;
    self
  }

  pub fn filetype(&self) -> &String {
    &self.filetype
  }


  pub fn set_sender(&mut self, sender: String) {
    self.sender = sender;
  }

  pub fn with_sender(mut self, sender: String) -> FileUploadInfo {
    self.sender = sender;
    self
  }

  pub fn sender(&self) -> &String {
    &self.sender
  }


  pub fn set_recipient(&mut self, recipient: String) {
    self.recipient = recipient;
  }

  pub fn with_recipient(mut self, recipient: String) -> FileUploadInfo {
    self.recipient = recipient;
    self
  }

  pub fn recipient(&self) -> &String {
    &self.recipient
  }


  pub fn set_date(&mut self, date: chrono::NaiveDate) {
    self.date = date;
  }

  pub fn with_date(mut self, date: chrono::NaiveDate) -> FileUploadInfo {
    self.date = date;
    self
  }

  pub fn date(&self) -> &chrono::NaiveDate {
    &self.date
  }


  pub fn set_time(&mut self, time: chrono::NaiveTime) {
    self.time = time;
  }

  pub fn with_time(mut self, time: chrono::NaiveTime) -> FileUploadInfo {
    self.time = time;
    self
  }

  pub fn time(&self) -> &chrono::NaiveTime {
    &self.time
  }


}



