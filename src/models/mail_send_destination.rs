/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct MailSendDestination {
  #[serde(rename = "name")]
  name: String,
  #[serde(rename = "address")]
  address: String
}

impl MailSendDestination {
  pub fn new(name: String, address: String) -> MailSendDestination {
    MailSendDestination {
      name: name,
      address: address
    }
  }

  pub fn set_name(&mut self, name: String) {
    self.name = name;
  }

  pub fn with_name(mut self, name: String) -> MailSendDestination {
    self.name = name;
    self
  }

  pub fn name(&self) -> &String {
    &self.name
  }


  pub fn set_address(&mut self, address: String) {
    self.address = address;
  }

  pub fn with_address(mut self, address: String) -> MailSendDestination {
    self.address = address;
    self
  }

  pub fn address(&self) -> &String {
    &self.address
  }


}



