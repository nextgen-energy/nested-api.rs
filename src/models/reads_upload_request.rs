/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct ReadsUploadRequest {
  /// The meter reads
  #[serde(rename = "reads")]
  reads: Vec<::models::ReadsUploadRead>,
  /// The authentication token
  #[serde(rename = "token")]
  token: String
}

impl ReadsUploadRequest {
  pub fn new(reads: Vec<::models::ReadsUploadRead>, token: String) -> ReadsUploadRequest {
    ReadsUploadRequest {
      reads: reads,
      token: token
    }
  }

  pub fn set_reads(&mut self, reads: Vec<::models::ReadsUploadRead>) {
    self.reads = reads;
  }

  pub fn with_reads(mut self, reads: Vec<::models::ReadsUploadRead>) -> ReadsUploadRequest {
    self.reads = reads;
    self
  }

  pub fn reads(&self) -> &Vec<::models::ReadsUploadRead> {
    &self.reads
  }


  pub fn set_token(&mut self, token: String) {
    self.token = token;
  }

  pub fn with_token(mut self, token: String) -> ReadsUploadRequest {
    self.token = token;
    self
  }

  pub fn token(&self) -> &String {
    &self.token
  }


}



