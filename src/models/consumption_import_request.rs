/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct ConsumptionImportRequest {
  /// The consumption from ETS
  #[serde(rename = "consumption")]
  consumption: Vec<::models::ConsumptionEntry>,
  /// The authentication token
  #[serde(rename = "token")]
  token: String
}

impl ConsumptionImportRequest {
  pub fn new(consumption: Vec<::models::ConsumptionEntry>, token: String) -> ConsumptionImportRequest {
    ConsumptionImportRequest {
      consumption: consumption,
      token: token
    }
  }

  pub fn set_consumption(&mut self, consumption: Vec<::models::ConsumptionEntry>) {
    self.consumption = consumption;
  }

  pub fn with_consumption(mut self, consumption: Vec<::models::ConsumptionEntry>) -> ConsumptionImportRequest {
    self.consumption = consumption;
    self
  }

  pub fn consumption(&self) -> &Vec<::models::ConsumptionEntry> {
    &self.consumption
  }


  pub fn set_token(&mut self, token: String) {
    self.token = token;
  }

  pub fn with_token(mut self, token: String) -> ConsumptionImportRequest {
    self.token = token;
    self
  }

  pub fn token(&self) -> &String {
    &self.token
  }


}



