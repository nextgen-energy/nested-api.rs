/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct BillingDataRequest {
  #[serde(rename = "connection")]
  connection: i64,
  #[serde(rename = "period")]
  period: ::models::DatePeriod,
  /// The authentication token
  #[serde(rename = "token")]
  token: String
}

impl BillingDataRequest {
  pub fn new(connection: i64, period: ::models::DatePeriod, token: String) -> BillingDataRequest {
    BillingDataRequest {
      connection: connection,
      period: period,
      token: token
    }
  }

  pub fn set_connection(&mut self, connection: i64) {
    self.connection = connection;
  }

  pub fn with_connection(mut self, connection: i64) -> BillingDataRequest {
    self.connection = connection;
    self
  }

  pub fn connection(&self) -> &i64 {
    &self.connection
  }


  pub fn set_period(&mut self, period: ::models::DatePeriod) {
    self.period = period;
  }

  pub fn with_period(mut self, period: ::models::DatePeriod) -> BillingDataRequest {
    self.period = period;
    self
  }

  pub fn period(&self) -> &::models::DatePeriod {
    &self.period
  }


  pub fn set_token(&mut self, token: String) {
    self.token = token;
  }

  pub fn with_token(mut self, token: String) -> BillingDataRequest {
    self.token = token;
    self
  }

  pub fn token(&self) -> &String {
    &self.token
  }


}



