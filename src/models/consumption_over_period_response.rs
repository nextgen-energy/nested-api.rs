/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct ConsumptionOverPeriodResponse {
  /// Error message, or null if there was no error
  #[serde(rename = "error")]
  error: Option<String>,
  /// The ICP list
  #[serde(rename = "list")]
  list: Option<Vec<::models::ConsumptionOverPeriod>>
}

impl ConsumptionOverPeriodResponse {
  pub fn new() -> ConsumptionOverPeriodResponse {
    ConsumptionOverPeriodResponse {
      error: None,
      list: None
    }
  }

  pub fn set_error(&mut self, error: String) {
    self.error = Some(error);
  }

  pub fn with_error(mut self, error: String) -> ConsumptionOverPeriodResponse {
    self.error = Some(error);
    self
  }

  pub fn error(&self) -> Option<&String> {
    self.error.as_ref()
  }

  pub fn reset_error(&mut self) {
    self.error = None;
  }

  pub fn set_list(&mut self, list: Vec<::models::ConsumptionOverPeriod>) {
    self.list = Some(list);
  }

  pub fn with_list(mut self, list: Vec<::models::ConsumptionOverPeriod>) -> ConsumptionOverPeriodResponse {
    self.list = Some(list);
    self
  }

  pub fn list(&self) -> Option<&Vec<::models::ConsumptionOverPeriod>> {
    self.list.as_ref()
  }

  pub fn reset_list(&mut self) {
    self.list = None;
  }

}



