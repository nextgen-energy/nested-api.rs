/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct ReadsUploadRead {
  #[serde(rename = "icp")]
  icp: String,
  #[serde(rename = "meter_serial_no")]
  meter_serial_no: String,
  #[serde(rename = "meter_register_no")]
  meter_register_no: i32,
  #[serde(rename = "read_date")]
  read_date: chrono::NaiveDate,
  #[serde(rename = "read")]
  read: ::models::BigDecimal,
  #[serde(rename = "estimate")]
  estimate: bool,
  #[serde(rename = "comment")]
  comment: String
}

impl ReadsUploadRead {
  pub fn new(icp: String, meter_serial_no: String, meter_register_no: i32, read_date: chrono::NaiveDate, read: ::models::BigDecimal, estimate: bool, comment: String) -> ReadsUploadRead {
    ReadsUploadRead {
      icp: icp,
      meter_serial_no: meter_serial_no,
      meter_register_no: meter_register_no,
      read_date: read_date,
      read: read,
      estimate: estimate,
      comment: comment
    }
  }

  pub fn set_icp(&mut self, icp: String) {
    self.icp = icp;
  }

  pub fn with_icp(mut self, icp: String) -> ReadsUploadRead {
    self.icp = icp;
    self
  }

  pub fn icp(&self) -> &String {
    &self.icp
  }


  pub fn set_meter_serial_no(&mut self, meter_serial_no: String) {
    self.meter_serial_no = meter_serial_no;
  }

  pub fn with_meter_serial_no(mut self, meter_serial_no: String) -> ReadsUploadRead {
    self.meter_serial_no = meter_serial_no;
    self
  }

  pub fn meter_serial_no(&self) -> &String {
    &self.meter_serial_no
  }


  pub fn set_meter_register_no(&mut self, meter_register_no: i32) {
    self.meter_register_no = meter_register_no;
  }

  pub fn with_meter_register_no(mut self, meter_register_no: i32) -> ReadsUploadRead {
    self.meter_register_no = meter_register_no;
    self
  }

  pub fn meter_register_no(&self) -> &i32 {
    &self.meter_register_no
  }


  pub fn set_read_date(&mut self, read_date: chrono::NaiveDate) {
    self.read_date = read_date;
  }

  pub fn with_read_date(mut self, read_date: chrono::NaiveDate) -> ReadsUploadRead {
    self.read_date = read_date;
    self
  }

  pub fn read_date(&self) -> &chrono::NaiveDate {
    &self.read_date
  }


  pub fn set_read(&mut self, read: ::models::BigDecimal) {
    self.read = read;
  }

  pub fn with_read(mut self, read: ::models::BigDecimal) -> ReadsUploadRead {
    self.read = read;
    self
  }

  pub fn read(&self) -> &::models::BigDecimal {
    &self.read
  }


  pub fn set_estimate(&mut self, estimate: bool) {
    self.estimate = estimate;
  }

  pub fn with_estimate(mut self, estimate: bool) -> ReadsUploadRead {
    self.estimate = estimate;
    self
  }

  pub fn estimate(&self) -> &bool {
    &self.estimate
  }


  pub fn set_comment(&mut self, comment: String) {
    self.comment = comment;
  }

  pub fn with_comment(mut self, comment: String) -> ReadsUploadRead {
    self.comment = comment;
    self
  }

  pub fn comment(&self) -> &String {
    &self.comment
  }


}



