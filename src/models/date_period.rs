/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct DatePeriod {
  #[serde(rename = "start")]
  start: chrono::NaiveDate,
  #[serde(rename = "end")]
  end: chrono::NaiveDate
}

impl DatePeriod {
  pub fn new(start: chrono::NaiveDate, end: chrono::NaiveDate) -> DatePeriod {
    DatePeriod {
      start: start,
      end: end
    }
  }

  pub fn set_start(&mut self, start: chrono::NaiveDate) {
    self.start = start;
  }

  pub fn with_start(mut self, start: chrono::NaiveDate) -> DatePeriod {
    self.start = start;
    self
  }

  pub fn start(&self) -> &chrono::NaiveDate {
    &self.start
  }


  pub fn set_end(&mut self, end: chrono::NaiveDate) {
    self.end = end;
  }

  pub fn with_end(mut self, end: chrono::NaiveDate) -> DatePeriod {
    self.end = end;
    self
  }

  pub fn end(&self) -> &chrono::NaiveDate {
    &self.end
  }


}



