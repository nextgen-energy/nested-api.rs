/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct IcpDayInfo {
  #[serde(rename = "retailer")]
  retailer: String,
  #[serde(rename = "profile")]
  profile: String,
  #[serde(rename = "submission_hhr")]
  submission_hhr: bool,
  #[serde(rename = "submission_nhh")]
  submission_nhh: bool
}

impl IcpDayInfo {
  pub fn new(retailer: String, profile: String, submission_hhr: bool, submission_nhh: bool) -> IcpDayInfo {
    IcpDayInfo {
      retailer: retailer,
      profile: profile,
      submission_hhr: submission_hhr,
      submission_nhh: submission_nhh
    }
  }

  pub fn set_retailer(&mut self, retailer: String) {
    self.retailer = retailer;
  }

  pub fn with_retailer(mut self, retailer: String) -> IcpDayInfo {
    self.retailer = retailer;
    self
  }

  pub fn retailer(&self) -> &String {
    &self.retailer
  }


  pub fn set_profile(&mut self, profile: String) {
    self.profile = profile;
  }

  pub fn with_profile(mut self, profile: String) -> IcpDayInfo {
    self.profile = profile;
    self
  }

  pub fn profile(&self) -> &String {
    &self.profile
  }


  pub fn set_submission_hhr(&mut self, submission_hhr: bool) {
    self.submission_hhr = submission_hhr;
  }

  pub fn with_submission_hhr(mut self, submission_hhr: bool) -> IcpDayInfo {
    self.submission_hhr = submission_hhr;
    self
  }

  pub fn submission_hhr(&self) -> &bool {
    &self.submission_hhr
  }


  pub fn set_submission_nhh(&mut self, submission_nhh: bool) {
    self.submission_nhh = submission_nhh;
  }

  pub fn with_submission_nhh(mut self, submission_nhh: bool) -> IcpDayInfo {
    self.submission_nhh = submission_nhh;
    self
  }

  pub fn submission_nhh(&self) -> &bool {
    &self.submission_nhh
  }


}



