/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct BillingDataDay {
  #[serde(rename = "day")]
  day: chrono::NaiveDate,
  #[serde(rename = "consumption")]
  consumption: ::std::collections::HashMap<String, ::models::BigDecimal>,
  #[serde(rename = "tariffs")]
  tariffs: Vec<::models::BillingDataTariff>
}

impl BillingDataDay {
  pub fn new(day: chrono::NaiveDate, consumption: ::std::collections::HashMap<String, ::models::BigDecimal>, tariffs: Vec<::models::BillingDataTariff>) -> BillingDataDay {
    BillingDataDay {
      day: day,
      consumption: consumption,
      tariffs: tariffs
    }
  }

  pub fn set_day(&mut self, day: chrono::NaiveDate) {
    self.day = day;
  }

  pub fn with_day(mut self, day: chrono::NaiveDate) -> BillingDataDay {
    self.day = day;
    self
  }

  pub fn day(&self) -> &chrono::NaiveDate {
    &self.day
  }


  pub fn set_consumption(&mut self, consumption: ::std::collections::HashMap<String, ::models::BigDecimal>) {
    self.consumption = consumption;
  }

  pub fn with_consumption(mut self, consumption: ::std::collections::HashMap<String, ::models::BigDecimal>) -> BillingDataDay {
    self.consumption = consumption;
    self
  }

  pub fn consumption(&self) -> &::std::collections::HashMap<String, ::models::BigDecimal> {
    &self.consumption
  }


  pub fn set_tariffs(&mut self, tariffs: Vec<::models::BillingDataTariff>) {
    self.tariffs = tariffs;
  }

  pub fn with_tariffs(mut self, tariffs: Vec<::models::BillingDataTariff>) -> BillingDataDay {
    self.tariffs = tariffs;
    self
  }

  pub fn tariffs(&self) -> &Vec<::models::BillingDataTariff> {
    &self.tariffs
  }


}



