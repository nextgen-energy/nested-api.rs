/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct IcpUpdateDetails {
  #[serde(rename = "icp")]
  icp: String,
  #[serde(rename = "gxp")]
  gxp: String,
  #[serde(rename = "price_code")]
  price_code: String,
  #[serde(rename = "dedicated_nsp")]
  dedicated_nsp: bool,
  #[serde(rename = "loss_code")]
  loss_code: String,
  #[serde(rename = "anzsic")]
  anzsic: String,
  #[serde(rename = "retailer")]
  retailer: String,
  #[serde(rename = "profile")]
  profile: String,
  #[serde(rename = "submission_hhr")]
  submission_hhr: bool,
  #[serde(rename = "submission_nhh")]
  submission_nhh: bool,
  #[serde(rename = "proposed_mep")]
  proposed_mep: String,
  #[serde(rename = "mep")]
  mep: String,
  #[serde(rename = "no_of_installations")]
  no_of_installations: i32,
  #[serde(rename = "installation_type")]
  installation_type: String,
  #[serde(rename = "generation_capacity")]
  generation_capacity: Option<::models::BigDecimal>,
  #[serde(rename = "capacity")]
  capacity: ::models::BigDecimal
}

impl IcpUpdateDetails {
  pub fn new(icp: String, gxp: String, price_code: String, dedicated_nsp: bool, loss_code: String, anzsic: String, retailer: String, profile: String, submission_hhr: bool, submission_nhh: bool, proposed_mep: String, mep: String, no_of_installations: i32, installation_type: String, capacity: ::models::BigDecimal) -> IcpUpdateDetails {
    IcpUpdateDetails {
      icp: icp,
      gxp: gxp,
      price_code: price_code,
      dedicated_nsp: dedicated_nsp,
      loss_code: loss_code,
      anzsic: anzsic,
      retailer: retailer,
      profile: profile,
      submission_hhr: submission_hhr,
      submission_nhh: submission_nhh,
      proposed_mep: proposed_mep,
      mep: mep,
      no_of_installations: no_of_installations,
      installation_type: installation_type,
      generation_capacity: None,
      capacity: capacity
    }
  }

  pub fn set_icp(&mut self, icp: String) {
    self.icp = icp;
  }

  pub fn with_icp(mut self, icp: String) -> IcpUpdateDetails {
    self.icp = icp;
    self
  }

  pub fn icp(&self) -> &String {
    &self.icp
  }


  pub fn set_gxp(&mut self, gxp: String) {
    self.gxp = gxp;
  }

  pub fn with_gxp(mut self, gxp: String) -> IcpUpdateDetails {
    self.gxp = gxp;
    self
  }

  pub fn gxp(&self) -> &String {
    &self.gxp
  }


  pub fn set_price_code(&mut self, price_code: String) {
    self.price_code = price_code;
  }

  pub fn with_price_code(mut self, price_code: String) -> IcpUpdateDetails {
    self.price_code = price_code;
    self
  }

  pub fn price_code(&self) -> &String {
    &self.price_code
  }


  pub fn set_dedicated_nsp(&mut self, dedicated_nsp: bool) {
    self.dedicated_nsp = dedicated_nsp;
  }

  pub fn with_dedicated_nsp(mut self, dedicated_nsp: bool) -> IcpUpdateDetails {
    self.dedicated_nsp = dedicated_nsp;
    self
  }

  pub fn dedicated_nsp(&self) -> &bool {
    &self.dedicated_nsp
  }


  pub fn set_loss_code(&mut self, loss_code: String) {
    self.loss_code = loss_code;
  }

  pub fn with_loss_code(mut self, loss_code: String) -> IcpUpdateDetails {
    self.loss_code = loss_code;
    self
  }

  pub fn loss_code(&self) -> &String {
    &self.loss_code
  }


  pub fn set_anzsic(&mut self, anzsic: String) {
    self.anzsic = anzsic;
  }

  pub fn with_anzsic(mut self, anzsic: String) -> IcpUpdateDetails {
    self.anzsic = anzsic;
    self
  }

  pub fn anzsic(&self) -> &String {
    &self.anzsic
  }


  pub fn set_retailer(&mut self, retailer: String) {
    self.retailer = retailer;
  }

  pub fn with_retailer(mut self, retailer: String) -> IcpUpdateDetails {
    self.retailer = retailer;
    self
  }

  pub fn retailer(&self) -> &String {
    &self.retailer
  }


  pub fn set_profile(&mut self, profile: String) {
    self.profile = profile;
  }

  pub fn with_profile(mut self, profile: String) -> IcpUpdateDetails {
    self.profile = profile;
    self
  }

  pub fn profile(&self) -> &String {
    &self.profile
  }


  pub fn set_submission_hhr(&mut self, submission_hhr: bool) {
    self.submission_hhr = submission_hhr;
  }

  pub fn with_submission_hhr(mut self, submission_hhr: bool) -> IcpUpdateDetails {
    self.submission_hhr = submission_hhr;
    self
  }

  pub fn submission_hhr(&self) -> &bool {
    &self.submission_hhr
  }


  pub fn set_submission_nhh(&mut self, submission_nhh: bool) {
    self.submission_nhh = submission_nhh;
  }

  pub fn with_submission_nhh(mut self, submission_nhh: bool) -> IcpUpdateDetails {
    self.submission_nhh = submission_nhh;
    self
  }

  pub fn submission_nhh(&self) -> &bool {
    &self.submission_nhh
  }


  pub fn set_proposed_mep(&mut self, proposed_mep: String) {
    self.proposed_mep = proposed_mep;
  }

  pub fn with_proposed_mep(mut self, proposed_mep: String) -> IcpUpdateDetails {
    self.proposed_mep = proposed_mep;
    self
  }

  pub fn proposed_mep(&self) -> &String {
    &self.proposed_mep
  }


  pub fn set_mep(&mut self, mep: String) {
    self.mep = mep;
  }

  pub fn with_mep(mut self, mep: String) -> IcpUpdateDetails {
    self.mep = mep;
    self
  }

  pub fn mep(&self) -> &String {
    &self.mep
  }


  pub fn set_no_of_installations(&mut self, no_of_installations: i32) {
    self.no_of_installations = no_of_installations;
  }

  pub fn with_no_of_installations(mut self, no_of_installations: i32) -> IcpUpdateDetails {
    self.no_of_installations = no_of_installations;
    self
  }

  pub fn no_of_installations(&self) -> &i32 {
    &self.no_of_installations
  }


  pub fn set_installation_type(&mut self, installation_type: String) {
    self.installation_type = installation_type;
  }

  pub fn with_installation_type(mut self, installation_type: String) -> IcpUpdateDetails {
    self.installation_type = installation_type;
    self
  }

  pub fn installation_type(&self) -> &String {
    &self.installation_type
  }


  pub fn set_generation_capacity(&mut self, generation_capacity: ::models::BigDecimal) {
    self.generation_capacity = Some(generation_capacity);
  }

  pub fn with_generation_capacity(mut self, generation_capacity: ::models::BigDecimal) -> IcpUpdateDetails {
    self.generation_capacity = Some(generation_capacity);
    self
  }

  pub fn generation_capacity(&self) -> Option<&::models::BigDecimal> {
    self.generation_capacity.as_ref()
  }

  pub fn reset_generation_capacity(&mut self) {
    self.generation_capacity = None;
  }

  pub fn set_capacity(&mut self, capacity: ::models::BigDecimal) {
    self.capacity = capacity;
  }

  pub fn with_capacity(mut self, capacity: ::models::BigDecimal) -> IcpUpdateDetails {
    self.capacity = capacity;
    self
  }

  pub fn capacity(&self) -> &::models::BigDecimal {
    &self.capacity
  }


}



