/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct DataImportCustomer {
  #[serde(rename = "id")]
  id: i64,
  #[serde(rename = "name")]
  name: String
}

impl DataImportCustomer {
  pub fn new(id: i64, name: String) -> DataImportCustomer {
    DataImportCustomer {
      id: id,
      name: name
    }
  }

  pub fn set_id(&mut self, id: i64) {
    self.id = id;
  }

  pub fn with_id(mut self, id: i64) -> DataImportCustomer {
    self.id = id;
    self
  }

  pub fn id(&self) -> &i64 {
    &self.id
  }


  pub fn set_name(&mut self, name: String) {
    self.name = name;
  }

  pub fn with_name(mut self, name: String) -> DataImportCustomer {
    self.name = name;
    self
  }

  pub fn name(&self) -> &String {
    &self.name
  }


}



