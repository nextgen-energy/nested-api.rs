/* 
 * NextGen Energy Ltd - API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: staging
 * Contact: info@nextgen.energy
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


#[allow(unused_imports)]
use serde_json::Value;

#[derive(Debug, Serialize, Deserialize)]
pub struct EmployeeListResponse {
  /// Error message, or null if there was no error
  #[serde(rename = "error")]
  error: Option<String>,
  /// The employee list
  #[serde(rename = "employees")]
  employees: Option<Vec<::models::EmployeeListEntry>>
}

impl EmployeeListResponse {
  pub fn new() -> EmployeeListResponse {
    EmployeeListResponse {
      error: None,
      employees: None
    }
  }

  pub fn set_error(&mut self, error: String) {
    self.error = Some(error);
  }

  pub fn with_error(mut self, error: String) -> EmployeeListResponse {
    self.error = Some(error);
    self
  }

  pub fn error(&self) -> Option<&String> {
    self.error.as_ref()
  }

  pub fn reset_error(&mut self) {
    self.error = None;
  }

  pub fn set_employees(&mut self, employees: Vec<::models::EmployeeListEntry>) {
    self.employees = Some(employees);
  }

  pub fn with_employees(mut self, employees: Vec<::models::EmployeeListEntry>) -> EmployeeListResponse {
    self.employees = Some(employees);
    self
  }

  pub fn employees(&self) -> Option<&Vec<::models::EmployeeListEntry>> {
    self.employees.as_ref()
  }

  pub fn reset_employees(&mut self) {
    self.employees = None;
  }

}



