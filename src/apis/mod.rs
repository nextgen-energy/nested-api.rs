use hyper;
use serde_json;

#[derive(Debug)]
pub enum Error {
    Hyper(hyper::Error),
    Serde(serde_json::Error),
}

impl From<hyper::Error> for Error {
    fn from(e: hyper::Error) -> Self {
        return Error::Hyper(e)
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        return Error::Serde(e)
    }
}

use super::models::*;

mod aws_api;
pub use self::aws_api::{ AwsApi, AwsApiClient };
mod internal_api;
pub use self::internal_api::{ InternalApi, InternalApiClient };
mod public_api;
pub use self::public_api::{ PublicApi, PublicApiClient };
mod service_api;
pub use self::service_api::{ ServiceApi, ServiceApiClient };

pub mod configuration;
pub mod client;
