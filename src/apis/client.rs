use std::rc::Rc;

use hyper;
use super::configuration::Configuration;

pub struct APIClient<C: hyper::client::Connect> {
  configuration: Rc<Configuration<C>>,
  aws_api: Box<::apis::AwsApi>,
  internal_api: Box<::apis::InternalApi>,
  public_api: Box<::apis::PublicApi>,
  service_api: Box<::apis::ServiceApi>,
}

impl<C: hyper::client::Connect> APIClient<C> {
  pub fn new(configuration: Configuration<C>) -> APIClient<C> {
    let rc = Rc::new(configuration);

    APIClient {
      configuration: rc.clone(),
      aws_api: Box::new(::apis::AwsApiClient::new(rc.clone())),
      internal_api: Box::new(::apis::InternalApiClient::new(rc.clone())),
      public_api: Box::new(::apis::PublicApiClient::new(rc.clone())),
      service_api: Box::new(::apis::ServiceApiClient::new(rc.clone())),
    }
  }

  pub fn aws_api(&self) -> &::apis::AwsApi{
    self.aws_api.as_ref()
  }

  pub fn internal_api(&self) -> &::apis::InternalApi{
    self.internal_api.as_ref()
  }

  pub fn public_api(&self) -> &::apis::PublicApi{
    self.public_api.as_ref()
  }

  pub fn service_api(&self) -> &::apis::ServiceApi{
    self.service_api.as_ref()
  }


}
